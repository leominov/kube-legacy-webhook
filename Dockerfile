FROM golang:1.16-alpine3.13 AS builder
WORKDIR /go/src/gitlab.qleanlabs.ru/platform/infra/kube-legacy-webhook
COPY . .
RUN go build .

FROM alpine:3.13
COPY --from=builder /go/src/gitlab.qleanlabs.ru/platform/infra/kube-legacy-webhook/kube-legacy-webhook /
ENTRYPOINT [ "/kube-legacy-webhook" ]
