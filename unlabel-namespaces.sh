#!/bin/bash

namespaces=$(kubectl get namespaces --no-headers -o custom-columns=NAME:.metadata.name | grep app)
for ns in ${namespaces[*]}; do
  echo "===> ${ns}"
  kubectl label namespace "${ns}" qlean.ru/kube-legacy-webhook-
done
