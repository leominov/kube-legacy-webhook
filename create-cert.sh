#!/bin/bash

WEBHOOK_NS=${1:-"default"}
WEBHOOK_NAME=${2:-"kube-legacy-webhook"}

# Create certs for our webhook
openssl genrsa -out webhookCA.key 2048
openssl req -new -key ./webhookCA.key -subj "/CN=${WEBHOOK_NAME}.${WEBHOOK_NS}.svc" -out ./webhookCA.csr
openssl x509 -req -days 365 -in webhookCA.csr -signkey webhookCA.key -out webhook.crt

# Create certs secrets for k8s
kubectl create secret generic \
    ${WEBHOOK_NAME} \
    --from-file=key.pem=./webhookCA.key \
    --from-file=cert.pem=./webhook.crt \
    --dry-run -o yaml > ./${WEBHOOK_NAME}.Secret.yaml
