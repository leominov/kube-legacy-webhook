package main

import (
	"context"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/slok/kubewebhook/v2/pkg/webhook/mutating"
	"github.com/stretchr/testify/assert"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/utils/pointer"
)

func TestDeploymentMutator_MutateSecret(t *testing.T) {
	m := NewMutator(logrus.WithField("app", "test"), &Config{})
	result, err := m.Mutate(context.Background(), nil, &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name: "foobar",
		},
		StringData: map[string]string{
			"foo": "bar",
		},
	})
	if assert.NoError(t, err) {
		assert.Equal(t, &mutating.MutatorResult{}, result)
	}
}

func TestDeploymentMutator_MutateDeployment_ReplicasRequest(t *testing.T) {
	tests := []struct {
		obj    metav1.Object
		expObj metav1.Object
	}{
		{
			obj: &appsv1.Deployment{
				Spec: appsv1.DeploymentSpec{
					Template: corev1.PodTemplateSpec{
						Spec: corev1.PodSpec{
							Containers: []corev1.Container{
								{
									Name: "foobar",
								},
							},
						},
					},
				},
			},
			expObj: &appsv1.Deployment{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						webhookAnnotation: "true",
					},
				},
				Spec: appsv1.DeploymentSpec{
					Replicas: pointer.Int32(2),
					Template: corev1.PodTemplateSpec{
						Spec: corev1.PodSpec{
							Containers: []corev1.Container{
								{
									Name: "foobar",
								},
							},
						},
					},
				},
			},
		},
		{
			obj: &appsv1.Deployment{
				Spec: appsv1.DeploymentSpec{
					Replicas: pointer.Int32(3),
					Template: corev1.PodTemplateSpec{
						Spec: corev1.PodSpec{
							Containers: []corev1.Container{
								{
									Name: "foobar",
								},
							},
						},
					},
				},
			},
		},
	}
	m := &Mutator{
		logger: logrus.WithField("app", "test"),
		config: &Config{
			ReplicasRequest:         2,
			AnnotateModifiedObjects: true,
			Mutations: map[string]map[string]bool{
				".*": {
					ReplicasLimitMutation:   false,
					ReplicasRequestMutation: true,
				},
			},
		},
	}
	for caseID, test := range tests {
		result, err := m.Mutate(context.Background(), nil, test.obj)
		if !assert.NoError(t, err) {
			continue
		}
		assert.Equal(t, test.expObj, result.MutatedObject, caseID)
	}
}

func TestDeploymentMutator_MutateDeployment(t *testing.T) {
	tests := []struct {
		obj    metav1.Object
		expObj metav1.Object
	}{
		{
			obj: &appsv1.Deployment{
				Spec: appsv1.DeploymentSpec{
					Replicas: pointer.Int32(1),
					Template: corev1.PodTemplateSpec{
						Spec: corev1.PodSpec{
							Containers: []corev1.Container{
								{
									Name: "foobar",
								},
							},
						},
					},
				},
			},
		},
		{
			obj: &appsv1.Deployment{
				Spec: appsv1.DeploymentSpec{
					Replicas: pointer.Int32(3),
					Template: corev1.PodTemplateSpec{
						Spec: corev1.PodSpec{
							Containers: []corev1.Container{
								{
									Name: "foobar",
								},
							},
						},
					},
				},
			},
			expObj: &appsv1.Deployment{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						webhookAnnotation: "true",
					},
				},
				Spec: appsv1.DeploymentSpec{
					Replicas: pointer.Int32(1),
					Template: corev1.PodTemplateSpec{
						Spec: corev1.PodSpec{
							Containers: []corev1.Container{
								{
									Name: "foobar",
								},
							},
						},
					},
				},
			},
		},
	}
	m := &Mutator{
		logger: logrus.WithField("app", "test"),
		config: &Config{
			ReplicasLimit:           1,
			AnnotateModifiedObjects: true,
		},
	}
	for caseID, test := range tests {
		result, err := m.Mutate(context.Background(), nil, test.obj)
		if !assert.NoError(t, err) {
			continue
		}
		assert.Equal(t, test.expObj, result.MutatedObject, caseID)
	}
}

func TestDeploymentMutator_MutatePod(t *testing.T) {
	tests := []struct {
		annotate bool
		obj      metav1.Object
		expObj   metav1.Object
	}{
		{
			annotate: true,
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: tillerDeprecatedImage + ":1.2.3",
						},
					},
				},
			},
		},
		{
			annotate: true,
			obj: &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						"foo":             "bar",
						webhookAnnotation: "true",
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "tiller",
							Image: tillerDeprecatedImage + ":1.2.3",
						},
					},
				},
			},
			expObj: &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						"foo":             "bar",
						webhookAnnotation: "true",
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "tiller",
							Image: "eu.gcr.io/tiller:1.2.3",
						},
					},
				},
			},
		},
		{
			annotate: false,
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "tiller",
							Image: tillerDeprecatedImage + ":1.2.3",
						},
					},
				},
			},
			expObj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "tiller",
							Image: "eu.gcr.io/tiller:1.2.3",
						},
					},
				},
			},
		},
		{
			annotate: true,
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "tiller",
							Image: "eu.gcr.io/tiller:1.2.3",
							Resources: corev1.ResourceRequirements{
								Requests: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceCPU: resource.MustParse("50m"),
								},
							},
						},
					},
				},
			},
		},
		{
			annotate: true,
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "tiller",
							Image: "eu.gcr.io/tiller:1.2.3",
							Resources: corev1.ResourceRequirements{
								Requests: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceMemory: resource.MustParse("512M"),
									corev1.ResourceCPU:    resource.MustParse("350m"),
								},
							},
						},
					},
				},
			},
			expObj: &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						webhookAnnotation: "true",
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "tiller",
							Image: "eu.gcr.io/tiller:1.2.3",
							Resources: corev1.ResourceRequirements{
								Requests: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceMemory: resource.MustParse("512M"),
									corev1.ResourceCPU:    *resource.NewMilliQuantity(100, resource.DecimalSI),
								},
							},
						},
					},
				},
			},
		},
		{
			annotate: true,
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "backend",
							Image: "eu.gcr.io/backend:1.2.3",
						},
					},
				},
			},
			expObj: &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						webhookAnnotation: "true",
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "backend",
							Image: "eu.gcr.io/backend:1.2.3",
							SecurityContext: &corev1.SecurityContext{
								Capabilities: &corev1.Capabilities{
									Drop: []corev1.Capability{
										"ALL",
									},
								},
								RunAsUser:              pointer.Int64(1001),
								RunAsNonRoot:           pointer.Bool(true),
								ReadOnlyRootFilesystem: pointer.Bool(true),
							},
						},
					},
				},
			},
		},
		{
			annotate: true,
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "backend",
							Image: "eu.gcr.io/backend:1.2.3",
							SecurityContext: &corev1.SecurityContext{
								Capabilities: &corev1.Capabilities{
									Drop: []corev1.Capability{
										"ALL",
									},
								},
								RunAsUser:              pointer.Int64(1001),
								RunAsNonRoot:           pointer.Bool(true),
								ReadOnlyRootFilesystem: pointer.Bool(true),
							},
						},
						{
							Name:  "foobar",
							Image: "eu.gcr.io/foobar:1.2.3",
						},
					},
				},
			},
		},
	}
	m := &Mutator{
		logger: logrus.WithField("app", "test"),
		config: &Config{
			ContainerRequestCPULimit: 100,
			TillerContainerImage:     "eu.gcr.io/tiller",
			TillerContainerName:      "tiller",
		},
	}
	for caseID, test := range tests {
		m.config.AnnotateModifiedObjects = test.annotate
		result, err := m.Mutate(context.Background(), nil, test.obj)
		if !assert.NoError(t, err) {
			continue
		}
		assert.Equal(t, test.expObj, result.MutatedObject, caseID)
	}
}

func TestDeploymentMutator_MutatePod_RemoveCPULimit(t *testing.T) {
	tests := []struct {
		mutations map[string]bool
		obj       metav1.Object
		expObj    metav1.Object
	}{
		{
			mutations: map[string]bool{
				ContainerLimitCPURemoveMutation: true,
			},
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
							Resources: corev1.ResourceRequirements{
								Limits: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceCPU: resource.MustParse("200m"),
								},
							},
						},
					},
				},
			},
			expObj: &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						webhookAnnotation: "true",
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
							Resources: corev1.ResourceRequirements{
								Limits: map[corev1.ResourceName]resource.Quantity{},
							},
						},
					},
				},
			},
		},
		{
			mutations: map[string]bool{
				ContainerLimitCPURemoveMutation: true,
			},
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
							Resources: corev1.ResourceRequirements{
								Limits: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceCPU:    resource.MustParse("200m"),
									corev1.ResourceMemory: resource.MustParse("512Mi"),
								},
							},
						},
					},
				},
			},
			expObj: &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						webhookAnnotation: "true",
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
							Resources: corev1.ResourceRequirements{
								Limits: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceMemory: resource.MustParse("512Mi"),
								},
							},
						},
					},
				},
			},
		},
		{
			mutations: map[string]bool{
				ContainerLimitCPURemoveMutation: false,
			},
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
							Resources: corev1.ResourceRequirements{
								Limits: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceCPU:    resource.MustParse("200m"),
									corev1.ResourceMemory: resource.MustParse("512Mi"),
								},
							},
						},
					},
				},
			},
		},
		{
			mutations: map[string]bool{
				ContainerLimitCPURemoveMutation: true,
			},
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
							Resources: corev1.ResourceRequirements{
								Limits: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceMemory: resource.MustParse("512Mi"),
								},
							},
						},
					},
				},
			},
		},
	}
	m := &Mutator{
		logger: logrus.WithField("app", "test"),
		config: &Config{
			AnnotateModifiedObjects: true,
		},
	}
	for caseID, test := range tests {
		m.config.Mutations = map[string]map[string]bool{
			".*": test.mutations,
		}
		result, err := m.Mutate(context.Background(), nil, test.obj)
		if !assert.NoError(t, err) {
			continue
		}
		assert.Equal(t, test.expObj, result.MutatedObject, caseID)
	}
}

func TestDeploymentMutator_MutatePod_Memory(t *testing.T) {
	tests := []struct {
		limit     string
		mutations map[string]bool
		obj       metav1.Object
		expObj    metav1.Object
	}{
		{
			limit: "foobar",
			mutations: map[string]bool{
				ContainerRequestMemoryLimitMutation: true,
			},
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
							Resources: corev1.ResourceRequirements{
								Requests: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceMemory: resource.MustParse("512M"),
								},
							},
						},
					},
				},
			},
		},
		{
			limit: "200M",
			mutations: map[string]bool{
				ContainerRequestMemoryLimitMutation: true,
			},
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
						},
					},
				},
			},
		},
		{
			limit: "200M",
			mutations: map[string]bool{
				ContainerRequestMemoryLimitMutation: false,
			},
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
							Resources: corev1.ResourceRequirements{
								Requests: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceMemory: resource.MustParse("512M"),
								},
							},
						},
					},
				},
			},
		},
		{
			limit: "200M",
			mutations: map[string]bool{
				ContainerRequestMemoryLimitMutation: true,
			},
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
							Resources: corev1.ResourceRequirements{
								Requests: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceMemory: resource.MustParse("100M"),
								},
							},
						},
					},
				},
			},
		},
		{
			limit: "200M",
			mutations: map[string]bool{
				ContainerRequestMemoryLimitMutation: true,
			},
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
							Resources: corev1.ResourceRequirements{
								Requests: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceMemory: resource.MustParse("512M"),
								},
							},
						},
					},
				},
			},
			expObj: &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						webhookAnnotation: "true",
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
							Resources: corev1.ResourceRequirements{
								Requests: map[corev1.ResourceName]resource.Quantity{
									corev1.ResourceMemory: resource.MustParse("200M"),
								},
							},
						},
					},
				},
			},
		},
	}
	m := &Mutator{
		logger: logrus.WithField("app", "test"),
		config: &Config{
			AnnotateModifiedObjects: true,
		},
	}
	for caseID, test := range tests {
		m.config.ContainerRequestMemoryLimit = test.limit
		m.config.Mutations = map[string]map[string]bool{
			".*": test.mutations,
		}
		result, err := m.Mutate(context.Background(), nil, test.obj)
		if !assert.NoError(t, err) {
			continue
		}
		assert.Equal(t, test.expObj, result.MutatedObject, caseID)
	}
}

func TestDeploymentMutator_MutatePod_ChangeImage(t *testing.T) {
	tests := []struct {
		mutations map[string]bool
		obj       metav1.Object
		expObj    metav1.Object
	}{
		{
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
						},
					},
				},
			},
		},
		{
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:master",
						},
					},
				},
			},
			expObj: &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						webhookAnnotation: "true",
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "master:flomaster",
						},
					},
				},
			},
		},
		{
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					InitContainers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:master",
						},
					},
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
						},
					},
				},
			},
			expObj: &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						webhookAnnotation: "true",
					},
				},
				Spec: corev1.PodSpec{
					InitContainers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "master:flomaster",
						},
					},
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
						},
					},
				},
			},
		},
	}
	m := &Mutator{
		logger: logrus.WithField("app", "test"),
		config: &Config{
			Images: []*Image{
				{
					Name:    "foobar:master",
					NewName: "master:flomaster",
				},
			},
			AnnotateModifiedObjects: true,
		},
	}
	for caseID, test := range tests {
		m.config.Mutations = map[string]map[string]bool{
			".*": {
				ChangeImageMutation: true,
			},
		}
		result, err := m.Mutate(context.Background(), nil, test.obj)
		if !assert.NoError(t, err) {
			continue
		}
		assert.Equal(t, test.expObj, result.MutatedObject, caseID)
	}
}

func TestDeploymentMutator_MutatePod_InternalRegistry(t *testing.T) {
	tests := []struct {
		mutations map[string]bool
		obj       metav1.Object
		expObj    metav1.Object
	}{
		{
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
						},
					},
				},
			},
		},
		{
			mutations: map[string]bool{
				InternalContainerRegistryMutation: true,
			},
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
						},
					},
				},
			},
			expObj: &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						webhookAnnotation: "true",
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "eu.gcr.io/mirror/foobar:1.2.3",
						},
					},
				},
			},
		},
		{
			mutations: map[string]bool{
				InternalContainerRegistryMutation: true,
			},
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					InitContainers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "foobar:1.2.3",
						},
					},
				},
			},
			expObj: &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						webhookAnnotation: "true",
					},
				},
				Spec: corev1.PodSpec{
					InitContainers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "eu.gcr.io/mirror/foobar:1.2.3",
						},
					},
				},
			},
		},
		{
			mutations: map[string]bool{
				InternalContainerRegistryMutation: true,
			},
			obj: &corev1.Pod{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "foobar",
							Image: "eu.gcr.io/foobar:1.2.3",
						},
					},
				},
			},
		},
	}
	m := &Mutator{
		logger: logrus.WithField("app", "test"),
		config: &Config{
			InternalContainerRegistry:     "eu.gcr.io",
			InternalContainerRegistryPath: "mirror",
			AnnotateModifiedObjects:       true,
		},
	}
	for caseID, test := range tests {
		m.config.Mutations = map[string]map[string]bool{
			".*": test.mutations,
		}
		result, err := m.Mutate(context.Background(), nil, test.obj)
		if !assert.NoError(t, err) {
			continue
		}
		assert.Equal(t, test.expObj, result.MutatedObject, caseID)
	}
}

func TestDeploymentMutator_MutateConfigMap(t *testing.T) {
	tests := []struct {
		annotate  bool
		mutations map[string]bool
		obj       metav1.Object
		expObj    metav1.Object
	}{
		{
			annotate:  true,
			mutations: defaultMutations,
			obj: &corev1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name:        "foobar",
					Annotations: nil,
				},
			},
		},
		{
			annotate:  true,
			mutations: defaultMutations,
			obj: &corev1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name: "foobar",
					Annotations: map[string]string{
						"ci_project_id": "123",
						"pltf_ttl":      "7d",
					},
				},
			},
		},
		{
			annotate:  true,
			mutations: defaultMutations,
			obj: &corev1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name: "foobar",
					Annotations: map[string]string{
						"ci_project_id": "123",
					},
				},
			},
			expObj: &corev1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name: "foobar",
					Annotations: map[string]string{
						"ci_project_id":   "123",
						"pltf_ttl":        "14d",
						webhookAnnotation: "true",
					},
				},
			},
		},
		{
			annotate: false,
			mutations: map[string]bool{
				DefaultHelmReleaseTTLMutation: true,
			},
			obj: &corev1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name: "foobar",
					Annotations: map[string]string{
						"ci_project_id": "123",
					},
				},
			},
			expObj: &corev1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name: "foobar",
					Annotations: map[string]string{
						"ci_project_id": "123",
						"pltf_ttl":      "14d",
					},
				},
			},
		},
		{
			annotate: false,
			obj: &corev1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name: "foobar",
					Annotations: map[string]string{
						"ci_project_id": "123",
					},
				},
			},
		},
	}
	m := &Mutator{
		logger: logrus.WithField("app", "test"),
		config: &Config{
			DefaultHelmReleaseTTL: "14d",
		},
	}
	for caseID, test := range tests {
		m.config.AnnotateModifiedObjects = test.annotate
		if len(test.mutations) == 0 {
			m.config.Mutations = map[string]map[string]bool{
				".*": {},
			}
		}
		result, err := m.Mutate(context.Background(), nil, test.obj)
		if !assert.NoError(t, err) {
			continue
		}
		assert.Equal(t, test.expObj, result.MutatedObject, caseID)
	}
}
