#!/bin/bash

namespaces=$(kubectl get namespaces --no-headers -l qlean.ru/kube-legacy-webhook=true -o custom-columns=NAME:.metadata.name)
for ns in ${namespaces[*]}; do
  echo "== ${ns}"
  deploys=$(kubectl -n "${ns}" get deploy --no-headers -l name!=tiller -o custom-columns=NAME:.metadata.name)
  for deploy in ${deploys[*]}; do
    echo "-- ${deploy}"
    kubectl -n "${ns}" get deploy "${deploy}" -o json | jq -r ".spec.template.spec.containers[].name"
  done
done