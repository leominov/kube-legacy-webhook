# kube-legacy-webhook

* [![App Status](https://argo.infra.cloud.qlean.ru/api/badge?name=kube-legacy-webhook-kosmos&revision=true)](https://argo.infra.cloud.qlean.ru/applications/kube-legacy-webhook-kosmos) – kosmos
* [![App Status](https://argo.infra.cloud.qlean.ru/api/badge?name=kube-legacy-webhook-preprod&revision=true)](https://argo.infra.cloud.qlean.ru/applications/kube-legacy-webhook-preprod) – preprod
* [![App Status](https://argo.infra.cloud.qlean.ru/api/badge?name=kube-legacy-webhook-stage&revision=true)](https://argo.infra.cloud.qlean.ru/applications/kube-legacy-webhook-stage) – stage

Изменения необходимые на момент переезда на новый CI касаются только приложений в пространствах имен с лейбом `qlean.ru/kube-legacy-webhook`.

Можно подключаться следующие модификации:

* Снижение числа реплик (`deployment`);
* Увеличение числа реплик (`deployment`) – отключено;
* Снижение реквестов CPU (`pod`);
* Снижение реквестов Memory (`pod`) – отключено;
* Исправление Docker-образа для Tiller (`pod`);
* Добавление аннотации `pltf_ttl` (`configmap`);
* Добавление Security Context (`pod`);
* Переключение на внутренний Docker Registry (`pod`) – отключено.

Модифицированным объектам будет добавлена аннотация `qlean.ru/kube-legacy-webhook-mutated`, в случае ошибок в работе webhook объекты будут созданы без изменений.

## Ссылки

* https://qleanru.atlassian.net/wiki/spaces/DOCS/pages/3140255777/Kube+Legacy+Webhook
* https://grafana.infra.cloud.qlean.ru/d/-i0yEBS7k/kubewebhook?orgId=1&refresh=30s&var-prometheus=%5BALL%5D%20Thanos&var-interval=$__auto_interval_interval&var-wh_version=All&var-mutating_wh=kube-legacy-webhook&var-validating_wh=All&var-range=$__auto_interval_range
