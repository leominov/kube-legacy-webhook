package main

import (
	"encoding/json"
	"os"
	"regexp"

	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
)

const (
	ChangeImageMutation                 = "change_image"
	ContainerLimitCPURemoveMutation     = "container_limit_cpu_remove"
	ContainerRequestCPULimitMutation    = "container_request_cpu_limit"
	ContainerRequestMemoryLimitMutation = "container_request_memory_limit"
	DefaultHelmReleaseTTLMutation       = "default_helm_release_ttl"
	InternalContainerRegistryMutation   = "internal_container_registry"
	ReplicasLimitMutation               = "replicas_limit"
	ReplicasRequestMutation             = "replicas_request"
	SecurityContextMutation             = "security_context"
	TillerContainerImageMutation        = "tiller_container_image"
)

var defaultMutations = map[string]bool{
	ChangeImageMutation:                 false,
	ContainerLimitCPURemoveMutation:     false,
	ContainerRequestCPULimitMutation:    true,
	ContainerRequestMemoryLimitMutation: false,
	DefaultHelmReleaseTTLMutation:       true,
	InternalContainerRegistryMutation:   false,
	ReplicasLimitMutation:               true,
	ReplicasRequestMutation:             false,
	SecurityContextMutation:             true,
	TillerContainerImageMutation:        true,
}

type Config struct {
	AnnotateModifiedObjects       bool                       `json:"annotate_modified_objects"`
	ContainerRequestCPULimit      int64                      `json:"container_request_cpu_limit"`
	ContainerRequestMemoryLimit   string                     `json:"container_request_memory_limit"`
	DefaultHelmReleaseTTL         string                     `json:"default_helm_release_ttl"`
	DefaultMutations              map[string]bool            `json:"default_mutations,omitempty"`
	Images                        []*Image                   `yaml:"images,omitempty"`
	InternalContainerRegistry     string                     `json:"internal_container_registry"`
	InternalContainerRegistryPath string                     `json:"internal_container_registry_path"`
	Mutations                     map[string]map[string]bool `json:"mutations,omitempty"`
	ReplicasLimit                 int32                      `json:"replicas_limit"`
	ReplicasRequest               int32                      `json:"replicas_request"`
	TillerContainerImage          string                     `json:"tiller_container_image"`
	TillerContainerName           string                     `json:"tiller_container_name"`
}

type MutationsConfig struct {
	DefaultMutations map[string]bool            `yaml:"default_mutations"`
	Mutations        map[string]map[string]bool `yaml:"mutations"`
	Images           []*Image                   `yaml:"images"`
}

type Image struct {
	Name    string `yaml:"name"`
	NewName string `yaml:"new_name"`
}

func SetDefaults() {
	viper.SetDefault("annotate_modified_objects", "true")
	viper.SetDefault("container_request_cpu_limit", "200")
	viper.SetDefault("container_request_memory_limit", "200M")
	viper.SetDefault("default_helm_release_ttl", "4d")
	viper.SetDefault("internal_container_registry", "eu.gcr.io/qlean-242314")
	viper.SetDefault("internal_container_registry_path", "gitlab/platform/infra")
	viper.SetDefault("listen_address", "0.0.0.0:8443")
	viper.SetDefault("log_format", "json")
	viper.SetDefault("log_level", "info")
	viper.SetDefault("replicas_limit", "1")
	viper.SetDefault("replicas_request", "2")
	viper.SetDefault("telemetry_listen_address", "0.0.0.0:8080")
	viper.SetDefault("tiller_container_image", "eu.gcr.io/qlean-242314/gitlab/platform/infra/tiller")
	viper.SetDefault("tiller_container_name", "tiller")
	viper.SetDefault("tls_cert_file", "")
	viper.SetDefault("tls_private_key_file", "")
	viper.AutomaticEnv()
}

func (c *Config) String() string {
	b, _ := json.Marshal(c)
	return string(b)
}

// ContainsMutation поиск мутации по ее имени и namespace. Если к конфигурации не
// определен список мутация для namespace, то будет использован список по-умолчанию,
// если namespace найден, но список мутаций пустой, считается, что все мутации отключены,
// если namespace найден, то список мутаций будет получен на основе списка по-умолчанию
// и дополнительных заданных настроек
func (c *Config) ContainsMutation(namespaceName, mutationName string) bool {
	var namespaceMutations = c.MutationsByNamespace(namespaceName)
	for mutation, enabled := range namespaceMutations {
		if mutation == mutationName && enabled {
			return true
		}
	}
	return false
}

func (c *Config) MutationsByNamespace(namespaceName string) map[string]bool {
	var namespaceMutations = make(map[string]bool)
	if len(c.DefaultMutations) > 0 {
		for name, enabled := range c.DefaultMutations {
			namespaceMutations[name] = enabled
		}
	} else {
		for name, enabled := range defaultMutations {
			namespaceMutations[name] = enabled
		}
	}

	for ns, mutations := range c.Mutations {
		match, err := regexp.MatchString(ns, namespaceName)
		if err != nil || !match {
			continue
		}
		if len(mutations) == 0 {
			namespaceMutations = mutations
		} else {
			for name, enabled := range mutations {
				namespaceMutations[name] = enabled
			}
		}
		break
	}
	return namespaceMutations
}

func LoadMutationsConfig(filename string) (*MutationsConfig, error) {
	b, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	mutationsConfig := &MutationsConfig{}
	err = yaml.Unmarshal(b, mutationsConfig)
	return mutationsConfig, err
}

func (c *Config) EnrichWithMutationsConfig(mutationsConfig *MutationsConfig) {
	if mutationsConfig == nil {
		return
	}
	c.DefaultMutations = mutationsConfig.DefaultMutations
	c.Mutations = mutationsConfig.Mutations
	c.Images = mutationsConfig.Images
}
