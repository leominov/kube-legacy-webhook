package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"text/tabwriter"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	kwhhttp "github.com/slok/kubewebhook/v2/pkg/http"
	whlog "github.com/slok/kubewebhook/v2/pkg/log/logrus"
	whmetrics "github.com/slok/kubewebhook/v2/pkg/metrics/prometheus"
	whwebhook "github.com/slok/kubewebhook/v2/pkg/webhook"
	"github.com/slok/kubewebhook/v2/pkg/webhook/mutating"
	"github.com/spf13/viper"
)

var (
	printsHelp              = flag.Bool("help", false, "Prints help and exit.")
	configPath              = flag.String("config", "config.yaml", "Path to configuration file.")
	debugMutations          = flag.Bool("debug", false, "Mutations debug.")
	debugMutationsNamespace = flag.String("debug-ns", "default", "Namespace for mutations debug.")
)

func init() {
	SetDefaults()
}

func main() {
	flag.Parse()

	var err error

	config := &Config{
		AnnotateModifiedObjects:       viper.GetBool("annotate_modified_objects"),
		ContainerRequestCPULimit:      viper.GetInt64("container_request_cpu_limit"),
		ContainerRequestMemoryLimit:   viper.GetString("container_request_memory_limit"),
		DefaultHelmReleaseTTL:         viper.GetString("default_helm_release_ttl"),
		InternalContainerRegistry:     viper.GetString("internal_container_registry"),
		InternalContainerRegistryPath: viper.GetString("internal_container_registry_path"),
		ReplicasLimit:                 viper.GetInt32("replicas_limit"),
		ReplicasRequest:               viper.GetInt32("replicas_request"),
		TillerContainerImage:          viper.GetString("tiller_container_image"),
		TillerContainerName:           viper.GetString("tiller_container_name"),
	}

	listenAddress := viper.GetString("listen_address")
	logFormat := viper.GetString("log_format")
	logLevel := viper.GetString("log_level")
	telemetryListenAddress := viper.GetString("telemetry_listen_address")
	tlsCertFile := viper.GetString("tls_cert_file")
	tlsPrivateKeyFile := viper.GetString("tls_private_key_file")

	var logger *logrus.Entry

	if *printsHelp {
		flag.Usage()
		fmt.Printf("\nEnvironment variables:\n\n")
		for name, val := range viper.AllSettings() {
			fmt.Printf("* %s=%v\n", strings.ToUpper(name), val)
		}
		return
	}

	l := logrus.New()

	switch logFormat {
	case "text":
		l.SetFormatter(&logrus.TextFormatter{})
	default:
		l.SetFormatter(&logrus.JSONFormatter{})
	}

	if level, err := logrus.ParseLevel(logLevel); err == nil {
		l.SetLevel(level)
	}

	logger = l.WithField("app", "kube-legacy-webhook")
	logger.Debugf("Log level: %s", l.Level.String())
	webhookLogger := whlog.NewLogrus(logger)
	mutator := NewMutator(logger, config)

	if mutationsConfig, err := LoadMutationsConfig(*configPath); err == nil {
		config.EnrichWithMutationsConfig(mutationsConfig)
	} else {
		logger.Warnf("Mutation configuration not loaded: %v", err)
	}

	if *debugMutations {
		mutations := config.MutationsByNamespace(*debugMutationsNamespace)
		w := new(tabwriter.Writer)
		w.Init(os.Stdout, 0, 8, 0, '\t', 0)
		_, _ = fmt.Fprintln(w, "Mutation\tEnabled")
		for name, enabled := range mutations {
			_, _ = fmt.Fprintf(w, "%s\t%v\n", name, enabled)
		}
		_ = w.Flush()
		return
	}

	webhook, err := mutating.NewWebhook(mutating.WebhookConfig{
		ID:      "kube-legacy-webhook",
		Mutator: mutator,
		Logger:  webhookLogger,
	})
	if err != nil {
		logger.Fatal(err)
	}

	metricsRecorder, err := whmetrics.NewRecorder(whmetrics.RecorderConfig{Registry: metricsRegistry})
	if err != nil {
		logger.Fatalf("error creating metrics recorder: %s", err)
	}
	promHandler := promhttp.HandlerFor(metricsRegistry, promhttp.HandlerOpts{})

	webhook = whwebhook.NewMeasuredWebhook(metricsRecorder, webhook)

	logger.Infof("Configuration: %s", config.String())

	go func() {
		telemetryMux := http.NewServeMux()
		telemetryMux.Handle("/metrics", promHandler)
		telemetryMux.HandleFunc("/healthz", func(w http.ResponseWriter, _ *http.Request) {
			w.WriteHeader(200)
		})
		logger.Infof("Telemetry on http://%s", telemetryListenAddress)
		logger.Fatal(http.ListenAndServe(telemetryListenAddress, telemetryMux))
	}()

	go func() {
		mux := http.NewServeMux()
		mux.Handle("/mutate", kwhhttp.MustHandlerFor(kwhhttp.HandlerConfig{
			Webhook: webhook,
			Logger:  webhookLogger,
		}))
		if tlsCertFile == "" && tlsPrivateKeyFile == "" {
			logger.Infof("Listening on http://%s", listenAddress)
			logger.Fatal(http.ListenAndServe(listenAddress, mux))
		}
		logger.Infof("Listening on https://%s", listenAddress)
		logger.Fatal(http.ListenAndServeTLS(listenAddress, tlsCertFile, tlsPrivateKeyFile, mux))
	}()

	shutdownSignalChan := make(chan os.Signal, 1)
	signal.Notify(shutdownSignalChan, syscall.SIGINT, syscall.SIGTERM)
	<-shutdownSignalChan

	logger.Info("Received shutdown signal")
}
