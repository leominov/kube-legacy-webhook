package main

import (
	"context"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/slok/kubewebhook/v2/pkg/model"
	"github.com/slok/kubewebhook/v2/pkg/webhook/mutating"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/utils/pointer"
)

const (
	tillerDeprecatedImage = "gcr.io/kubernetes-helm/tiller"
	webhookAnnotation     = "qlean.ru/kube-legacy-webhook-mutated"
)

type Mutator struct {
	config *Config
	logger *logrus.Entry
}

func NewMutator(logger *logrus.Entry, config *Config) *Mutator {
	return &Mutator{
		config: config,
		logger: logger,
	}
}

func (m *Mutator) Mutate(_ context.Context, ar *model.AdmissionReview, obj metav1.Object) (*mutating.MutatorResult, error) {
	var (
		mutated bool
		result  *mutating.MutatorResult
	)

	namespace := obj.GetNamespace()
	if ar != nil {
		namespace = ar.Namespace
	}

	log := m.logger.WithFields(logrus.Fields{
		"uid":       obj.GetUID(),
		"name":      obj.GetName(),
		"namespace": namespace,
	})

	log.Debugf("Available mutations: %v", m.config.MutationsByNamespace(namespace))

	switch v := obj.(type) {
	case *corev1.Pod:
		mutated, result = m.mutatePod(namespace, v)
	case *appsv1.Deployment:
		mutated, result = m.mutateDeployment(namespace, v)
	case *corev1.ConfigMap:
		mutated, result = m.mutateConfigMap(namespace, v)
	default:
		return &mutating.MutatorResult{}, nil
	}

	// Nothing changed
	if !mutated {
		return &mutating.MutatorResult{}, nil
	}

	if m.config.AnnotateModifiedObjects {
		annotations := obj.GetAnnotations()
		if annotations == nil {
			annotations = make(map[string]string)
		}
		annotations[webhookAnnotation] = "true"
		obj.SetAnnotations(annotations)
	}

	log.Info("Successfully mutated")

	return result, nil
}

func (m *Mutator) mutatePod(namespace string, pod *corev1.Pod) (bool, *mutating.MutatorResult) {
	var mutated bool

	if namespace == "" {
		namespace = pod.GetNamespace()
	}

	// Fix tiller image
	if m.config.ContainsMutation(namespace, TillerContainerImageMutation) {
		for id, container := range pod.Spec.Containers {
			if container.Name == m.config.TillerContainerName && strings.Contains(container.Image, tillerDeprecatedImage) {
				container.Image = strings.Replace(container.Image, tillerDeprecatedImage, m.config.TillerContainerImage, 1)
				pod.Spec.Containers[id] = container
				m.logger.WithField("container", container.Name).Debug("Updated due tiller image")
				mutated = true
			}
		}
	}

	// Fix cpu
	if m.config.ContainsMutation(namespace, ContainerRequestCPULimitMutation) {
		for id, container := range pod.Spec.Containers {
			cpu := container.Resources.Requests[corev1.ResourceCPU]
			if cpu.MilliValue() > m.config.ContainerRequestCPULimit {
				cpu.SetMilli(m.config.ContainerRequestCPULimit)
				pod.Spec.Containers[id].Resources.Requests[corev1.ResourceCPU] = cpu
				m.logger.WithField("container", container.Name).Debug("Updated due request CPU limit")
				mutated = true
			}
		}
	}

	// Fix memory
	if m.config.ContainsMutation(namespace, ContainerRequestMemoryLimitMutation) {
		memoryLimit, err := resource.ParseQuantity(m.config.ContainerRequestMemoryLimit)
		if err != nil {
			m.logger.WithError(err).Errorf("Failed to parse container memory limit: %s",
				m.config.ContainerRequestMemoryLimit)
		} else {
			for id, container := range pod.Spec.Containers {
				memory := container.Resources.Requests[corev1.ResourceMemory]
				if memory.Cmp(memoryLimit) == 1 {
					pod.Spec.Containers[id].Resources.Requests[corev1.ResourceMemory] = memoryLimit
					m.logger.WithField("container", container.Name).Debug("Updated due request memory limit")
					mutated = true
				}
			}
		}
	}

	// Fix security context
	if m.config.ContainsMutation(namespace, SecurityContextMutation) {
		for id, container := range pod.Spec.Containers {
			if container.Name != "frontend" && container.Name != "backend" && container.Name != "service" {
				continue
			}
			if container.SecurityContext != nil {
				continue
			}
			securityContext := &corev1.SecurityContext{
				Capabilities: &corev1.Capabilities{
					Drop: []corev1.Capability{
						"ALL",
					},
				},
				RunAsUser:              pointer.Int64(1001),
				RunAsNonRoot:           pointer.Bool(true),
				ReadOnlyRootFilesystem: pointer.Bool(true),
			}
			pod.Spec.Containers[id].SecurityContext = securityContext
			m.logger.WithField("container", container.Name).Debug("Updated due security context")
			mutated = true
		}
	}

	// Fix image registry
	if m.config.ContainsMutation(namespace, InternalContainerRegistryMutation) {
		for id, container := range pod.Spec.Containers {
			if strings.HasPrefix(container.Image, m.config.InternalContainerRegistry) {
				continue
			}
			imageName := path.Join(m.config.InternalContainerRegistry, m.config.InternalContainerRegistryPath, container.Image)
			pod.Spec.Containers[id].Image = imageName
			m.logger.WithField("container", container.Name).Debug("Updated due internal registry")
			mutated = true
		}
		for id, container := range pod.Spec.InitContainers {
			if strings.HasPrefix(container.Image, m.config.InternalContainerRegistry) {
				continue
			}
			imageName := path.Join(m.config.InternalContainerRegistry, m.config.InternalContainerRegistryPath, container.Image)
			pod.Spec.InitContainers[id].Image = imageName
			m.logger.WithField("initContainer", container.Name).Debug("Updated due internal registry")
			mutated = true
		}
	}

	// Update image by configuration from a file
	if m.config.ContainsMutation(namespace, ChangeImageMutation) {
		for id, container := range pod.Spec.Containers {
			for _, image := range m.config.Images {
				if image.Name != container.Image || image.NewName == "" {
					continue
				}
				pod.Spec.Containers[id].Image = image.NewName
				m.logger.WithField("container", container.Name).Debug("Updated due change image")
				mutated = true
			}
		}
		for id, container := range pod.Spec.InitContainers {
			for _, image := range m.config.Images {
				if image.Name != container.Image || image.NewName == "" {
					continue
				}
				pod.Spec.InitContainers[id].Image = image.NewName
				m.logger.WithField("initContainer", container.Name).Debug("Updated due change image")
				mutated = true
			}
		}
	}

	// Remove cpu
	if m.config.ContainsMutation(namespace, ContainerLimitCPURemoveMutation) {
		for id, container := range pod.Spec.Containers {
			cpu := container.Resources.Limits[corev1.ResourceCPU]
			if cpu.MilliValue() > 0 {
				delete(pod.Spec.Containers[id].Resources.Limits, corev1.ResourceCPU)
				mutated = true
			}
		}
	}

	return mutated, &mutating.MutatorResult{MutatedObject: pod}
}

func (m *Mutator) mutateDeployment(namespace string, deploy *appsv1.Deployment) (bool, *mutating.MutatorResult) {
	var mutated bool

	if namespace == "" {
		namespace = deploy.GetNamespace()
	}

	// Fix replicas by limit
	if m.config.ContainsMutation(deploy.GetNamespace(), ReplicasLimitMutation) {
		if deploy.Spec.Replicas != nil {
			if *deploy.Spec.Replicas > m.config.ReplicasLimit {
				deploy.Spec.Replicas = &m.config.ReplicasLimit
				m.logger.WithField("deployment", deploy.Name).Debug("Updated due replicas limit")
				mutated = true
			}
		}
	}

	// Fix replicas by request
	if m.config.ContainsMutation(deploy.GetNamespace(), ReplicasRequestMutation) {
		if deploy.Spec.Replicas == nil || *deploy.Spec.Replicas < m.config.ReplicasRequest {
			deploy.Spec.Replicas = &m.config.ReplicasRequest
			m.logger.WithField("deployment", deploy.Name).Debug("Updated due replicas request")
			mutated = true
		}
	}

	return mutated, &mutating.MutatorResult{MutatedObject: deploy}
}

func (m *Mutator) mutateConfigMap(namespace string, configMap *corev1.ConfigMap) (bool, *mutating.MutatorResult) {
	var mutated bool

	if namespace == "" {
		namespace = configMap.GetNamespace()
	}

	// Adds default environment TTL
	if m.config.ContainsMutation(configMap.GetNamespace(), DefaultHelmReleaseTTLMutation) {
		if configMap.Annotations != nil {
			_, projectIDExists := configMap.Annotations["ci_project_id"]
			_, ttlExists := configMap.Annotations["pltf_ttl"]
			if projectIDExists && !ttlExists {
				configMap.Annotations["pltf_ttl"] = m.config.DefaultHelmReleaseTTL
				m.logger.WithField("configMap", configMap.Name).Debug("Updated due default helm release TTL")
				mutated = true
			}
		}
	}

	return mutated, &mutating.MutatorResult{MutatedObject: configMap}
}
