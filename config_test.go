package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig_String(t *testing.T) {
	c := &Config{}
	assert.NotEmpty(t, c.String())
}

func TestConfig_ContainsMutation(t *testing.T) {
	tests := []struct {
		namespace        string
		mutation         string
		mutations        map[string]map[string]bool
		defaultMutations map[string]bool
		contains         bool
	}{
		{
			mutation: TillerContainerImageMutation,
			contains: true,
		},
		{
			mutation: "foobar",
			contains: false,
		},
		{
			namespace: "default",
			mutation:  SecurityContextMutation,
			contains:  true,
		},
		{
			namespace: "default",
			mutation:  SecurityContextMutation,
			contains:  false,
			defaultMutations: map[string]bool{
				SecurityContextMutation: false,
			},
		},
		{
			namespace: "app-web-widget-footer",
			mutation:  SecurityContextMutation,
			contains:  true,
			mutations: map[string]map[string]bool{
				"app-web-widget-+": {
					SecurityContextMutation: true,
				},
			},
			defaultMutations: map[string]bool{
				SecurityContextMutation: false,
			},
		},
		{
			namespace: "app-web-widget-footer",
			mutation:  SecurityContextMutation,
			contains:  true,
			mutations: map[string]map[string]bool{
				"app-web-widget-+": {
					SecurityContextMutation: true,
				},
			},
			defaultMutations: map[string]bool{
				TillerContainerImageMutation: false,
			},
		},
		{
			namespace: "default",
			mutation:  TillerContainerImageMutation,
			contains:  true,
			defaultMutations: map[string]bool{
				TillerContainerImageMutation: true,
				SecurityContextMutation:      false,
			},
		},
		{
			namespace: "default",
			mutation:  SecurityContextMutation,
			mutations: map[string]map[string]bool{
				"default": {
					SecurityContextMutation:      false,
					TillerContainerImageMutation: true,
				},
			},
			contains: false,
		},
		{
			namespace: "default",
			mutation:  SecurityContextMutation,
			mutations: map[string]map[string]bool{
				"default": {
					SecurityContextMutation: true,
				},
			},
			contains: true,
		},
		{
			namespace: "default",
			mutation:  SecurityContextMutation,
			mutations: map[string]map[string]bool{
				"default": {},
				"kube-system": {
					TillerContainerImageMutation: true,
				},
			},
			contains: false,
		},
		{
			namespace: "kube-system",
			mutation:  TillerContainerImageMutation,
			mutations: map[string]map[string]bool{
				"default": {},
				"kube-+": {
					TillerContainerImageMutation: true,
				},
			},
			contains: true,
		},
		{
			namespace: "kube-system",
			mutation:  TillerContainerImageMutation,
			mutations: map[string]map[string]bool{
				"default": {},
				"kube-+": {
					TillerContainerImageMutation: false,
				},
			},
			contains: false,
		},
	}
	for caseID, test := range tests {
		c := &Config{
			Mutations: test.mutations,
		}
		if len(test.defaultMutations) > 0 {
			c.DefaultMutations = test.defaultMutations
		}
		assert.Equal(t, test.contains, c.ContainsMutation(test.namespace, test.mutation), caseID)
	}
}

func TestLoadMutationsConfig(t *testing.T) {
	_, err := LoadMutationsConfig("test_data/not-found.yaml")
	assert.Error(t, err)
	mc, err := LoadMutationsConfig("test_data/valid-config.yaml")
	assert.NoError(t, err)
	c := &Config{}
	c.EnrichWithMutationsConfig(mc)
	assert.True(t, c.ContainsMutation("app-sso-foobar", ContainerRequestCPULimitMutation))
	assert.True(t, c.ContainsMutation("app-sso-foobar", SecurityContextMutation))
	assert.False(t, c.ContainsMutation("foobar", SecurityContextMutation))
}

func TestConfig_EnrichWithMutationsConfig(t *testing.T) {
	c := &Config{}
	c.EnrichWithMutationsConfig(nil)
	assert.Equal(t, 0, len(c.DefaultMutations))
	c.EnrichWithMutationsConfig(&MutationsConfig{
		DefaultMutations: defaultMutations,
	})
	assert.Equal(t, len(defaultMutations), len(c.DefaultMutations))
}
